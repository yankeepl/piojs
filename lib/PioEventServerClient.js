'use strict';

var util = require('util');
var Q = require('q');
var querystring = require('querystring');

var BasePioClient = require('./BasePioClient');

var EVENT_SERVER_URL_FORMAT = '%s/events%s.json?accessKey=%s';

module.exports = PioEventServerClient;

function PioEventServerClient(url, accessKey) {
	BasePioClient.call(this, url, accessKey);
}

util.inherits(PioEventServerClient, BasePioClient);

PioEventServerClient.prototype.getEventServerUrl = function(eventId) {
	eventId = (eventId) ? ('/'+eventId) : '';

	return util.format(EVENT_SERVER_URL_FORMAT, this.url, eventId, this.accessKey);
};

/**
* @returns {promise}
*/
PioEventServerClient.prototype.sendRequest = function(eventData, eventId, method) {
	method = method || 'POST';
	var httpOptions = {
		url: this.getEventServerUrl(eventId),
		timeout: this.getTimeout(),
		method: method,
		headers: {
			'Content-Type': 'application/json'
		}			
	};
	if (eventData) {
		if (method === 'GET') {
			httpOptions.url += '&' + querystring.stringify(eventData);
		} else {
			httpOptions.json = eventData; 
		}
	} 
	
	return this.sendJsonRequest(httpOptions);
};

PioEventServerClient.prototype.getEvent = function(eventId) {
	return this.sendRequest(null, eventId, 'GET');
};