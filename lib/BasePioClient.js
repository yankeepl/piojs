'use strict';

var url = require('url');
var httpRequest = require('request');
var Q = require('q');

var DEFAULT_TIMEOUT = 1000;
module.exports = BasePioClient;

function BasePioClient(sUrl, accessKey) {
	var parsedUrl = url.parse(sUrl);
	//normalize url
	this.url = url.format({
		protocol: parsedUrl.protocol,
		host: parsedUrl.host
	});
	this.accessKey = accessKey;
	this.timeout = DEFAULT_TIMEOUT;
}

BasePioClient.prototype.getTimeout = function() {
	return this.timeout;
};

BasePioClient.prototype.setTimeout = function(timeout) {
	this.timeout = timeout;
};

/**
 * @param {object} httpOptions
 * @returns {promise}
 */
BasePioClient.prototype.sendJsonRequest = function(httpOptions) {
	var deferred = Q.defer();
	httpRequest(httpOptions, function (err, res, data) {
		if (err) {
			deferred.reject(err);
		} else {
			switch(res.statusCode) {
				case 200:
				case 201:
					if ((typeof data) === 'string') {
						try {
							var oData = JSON.parse(data);
							deferred.resolve(oData);
						} catch (err) {
							deferred.reject(err);
						}
					} else {
						deferred.resolve(data);
					}
					break;
				case 404:
					deferred.resolve(null);
					break;
				default:
					deferred.reject(new Error('Http code: ' + res.statusCode + ', ' + data));
			}
		}
	});
	return deferred.promise;
};