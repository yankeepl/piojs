'use strict';

var util = require('util');

var CachedEntity = require('./CachedEntity');

module.exports = UserEntity;

function UserEntity(client) {
	CachedEntity.call(this, client);
	this.setType('user');
	this.preventDuplicates(true);
}

util.inherits(UserEntity, CachedEntity);