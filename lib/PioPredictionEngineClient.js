'use strict';

var util = require('util');
var httoRequest = require('request');
var Q = require('q');
var querystring = require('querystring');

var BasePioClient = require('./BasePioClient');

var EVENT_SERVER_URL_FORMAT = '%s/queries.json';

module.exports = PioPredictionEngineClient;

function PioPredictionEngineClient(url, accessKey) {
	BasePioClient.call(this, url, accessKey);
}

util.inherits(PioPredictionEngineClient, BasePioClient);

PioPredictionEngineClient.prototype.getServerUrl = function() {
	return util.format(EVENT_SERVER_URL_FORMAT, this.url);
};

/**
* @returns {promise}
*/
PioPredictionEngineClient.prototype.sendRequest = function(requestData, method) {
/*
curl -H "Content-Type: application/json" \
-d '{ "items": ["tchibo,2174754666"], "num": 4 }' \
http://localhost:8000/queries.json
 */	
	
	method = method || 'POST';
	var httpOptions = {
		url: this.getServerUrl(),
		timeout: this.getTimeout(),
		method: method,
		headers: {
			'Content-Type': 'application/json'
		}			
	};
	if (requestData) {
		if (method === 'POST') {
			httpOptions.json = requestData; 
		} else {
			throw new Error("unsupported method", method);
//			httpOptions.url += '&' + querystring.stringify(eventData);
		}
	} 

	return this.sendJsonRequest(httpOptions);
};

