'use strict';

var util = require('util');
var crypto = require('crypto');

var Q = require('q');
var PioEntity = require('./PioEntity');
// @TODO change to uber-cache
var _cache = {};

var createUberCache = require('uber-cache');
var uCache = createUberCache({
	"size": 50000
});
var cacheTTL = 1000 * 60 * 60 * 24 * 10; //1000ms * 60s * 60m * 24h * 10d

module.exports = CachedEntity;
/**
 * cache do not track Event.properties changes 
 * @returns {CachedEntity}
 */
function CachedEntity(client) {
	PioEntity.call(this, client);
	this.preventDuplicateEntities = false;	
}

util.inherits(CachedEntity, PioEntity);

CachedEntity.prototype._getCache = function(preventDuplicateEntities) {
	return uCache;
};

CachedEntity.prototype.preventDuplicates = function(preventDuplicateEntities) {
	this.preventDuplicateEntities = preventDuplicateEntities;
};

CachedEntity.prototype.send = function(entityId, customFields) {
	var that = this;
	return this
		//@TODO wpis w cacheu powinien tez zalezec od customFields
		._loadEntityFromCache(entityId, customFields)
		.then(function(eventId) {
			if (eventId) {
				return eventId;
			} else {
				return CachedEntity.super_.prototype.send.call(that, entityId, customFields)
					.then(function(eventId){
						var cacheId = that._getCacheId(entityId, customFields);
						that._saveEntityToCache(cacheId, eventId);
						return eventId;
					});
			}
		});
};

/**
 * @private
 */
CachedEntity.prototype._loadEntityFromCache = function(entityId, customFields) {
//	console.log('_loadEntityFromCache');
	var deferred = Q.defer();
	var that = this;
	if (this.preventDuplicateEntities) {
		var cacheId = this._getCacheId(entityId, customFields);
//		console.log('_loadEntityFromCache.1 - prevent duplicates', cacheId);
		that._getCache().get(cacheId, function(err, entityEventId){
			if (err) {
				deferred.reject(err);
				return;//--->>
			}
			if (entityEventId) {
//				console.log('_loadEntityFromCache.1.1 - found in cache');
				//gdy jest w cache to zwroc
				deferred.resolve(entityEventId);
			} else {
//				console.log('_loadEntityFromCache.1.2 - not exists in cache');
				//gdy nie ma to pobierz rzeczywista wartosc i zwroc
				that.retrive(entityId)
					.then(function(data){
						if (data) {
							entityEventId = data[0].eventId;
//							console.log('_loadEntityFromCache.1.2.1 - exists in db', entityEventId);
							that._saveEntityToCache(cacheId, entityEventId);
						} else {
//							console.log('_loadEntityFromCache.1.2.1 - not exists in db');
						}
						deferred.resolve(entityEventId);
					})
					.fail(function(err){
						deferred.reject(err);
					});
			}
			
		});
	} else {
//		console.log('_loadEntityFromCache.2');
		//gdy bez cache to zawsze zwraca pusta wartosc
		deferred.resolve(null);
	}
	return deferred.promise;
};

/**
 * @private
 */
CachedEntity.prototype._saveEntityToCache = function(cacheId, eventId) {
	//@TODO zapis do cache bedzie asynchroniczny wiec tez promesa
	if (this.preventDuplicateEntities) {
//		console.log('_saveEntityToCache', cacheId, eventId);
		this._getCache().set(cacheId, eventId, function(err) {
			if (err) {
				console.error(err);
				//ignore cache errors
			}
		});
	}
	return eventId;		
};

/**
 * @private
 */
CachedEntity.prototype._getCacheId = function(entityId, customFields) {
	var sId = entityId + '-' + JSON.stringify(customFields) + '-' + this.entityType;
	var hashedId = crypto.createHash('md5').update(sId).digest('hex') + '-CachedEntity';
	
	return hashedId;
};