'use strict';

var util = require('util');
var PioEntity = require('./PioEntity');
var UserEntity = require('./UserEntity');
var ItemEntity = require('./ItemEntity');

module.exports = UserToItemEvent;

function UserToItemEvent(client) {
	PioEntity.call(this, client);
	this.itemEntity = new ItemEntity(client);
	this.userEntity = new UserEntity(client);
	
	/*
	// @TODO events listener does not work?
	this.itemEntity.on('send-success', function(data){
		console.log('item sent', data);
	});
	this.userEntity.on('send-success', function(data){
		console.log('user sent', data);
	});
	*/
//	this.setEvent('view');
}

util.inherits(UserToItemEvent, PioEntity);

UserToItemEvent.prototype.sendEvent = function (userParams, event, itemParams) {
	var that = this;
	this.setEvent(event);
	this.setType('user');
	
	return this.userEntity
		.send(userParams.id, userParams.options)
		.then(function(data) {
//			console.log('user done', data);
		})
		.then(function(){
			return that.itemEntity.send(itemParams.id, itemParams.options);			
		})
		.then(function(data) {
//			console.log('item done', data);
		})
		.then(function(){
			return that.send(userParams.id, {
				'targetEntityType': 'item',
				'targetEntityId': itemParams.id
			});
		})
		.then(function(data) {
//			console.log('event done', data);
			return data;
		});
};