'use strict';

var util = require('util');

var CachedEntity = require('./CachedEntity');

module.exports = ItemEntity;

function ItemEntity(client) {
	CachedEntity.call(this, client);
	this.setType('item');
	this.preventDuplicates(true);
}

util.inherits(ItemEntity, CachedEntity);