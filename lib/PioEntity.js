'use strict';

var _ = require('lodash');
var util = require('util');
var EventEmitter = require('events').EventEmitter;

module.exports = PioEntity;
/**
 * 
 * @param {PioEventServerClient} pioEventServerClient
 * @returns {PioEntity}
 */
function PioEntity(pioEventServerClient) {
	EventEmitter.call(this);
	/**
	 * @property {PioEventServerClient}
	 */
	this.client = pioEventServerClient;
	this.entityType = '';
	this.event = '$set';
}

util.inherits(PioEntity, EventEmitter);

PioEntity.prototype.setType = function(entityType) {
	this.entityType = entityType;
};

PioEntity.prototype.setEvent = function(event) {
	this.event = event;
};

PioEntity.prototype.send = function(entityId, customFields) {
	var that = this;
	var safeCustomFields = customFields || {};
	var data = {
		"event": this.event,
		"entityType": this.entityType,
		"entityId": entityId
	};
	_.extend(data, safeCustomFields);
	
	return this.client
		.sendRequest( data, null, 'POST' )
		.then(function(data) {
			that.emit('send-success', data.eventId);
			return data.eventId;
		})
		.fail(function(error) {
			that.emit('send-fail', error);
			throw error;
		})
		;
};

PioEntity.prototype.retrive = function(entityId) {
	var data = {
		"entityType": this.entityType,
		"entityId": entityId
	};
	return this.client
		.sendRequest( data, null, 'GET' );
};