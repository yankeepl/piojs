'use strict';

var predictionClient = require('../index');
var _ = require('lodash');

//Example config format
//{
//	"accessKey": "SomeVeryLongAccessKeyGeneratedByPioAppNew",
//	"url": "http://example.com:7070/"
//}
var config = require('./config.json');

// setup client
var pioPredictionEngineClient = new predictionClient.PioPredictionEngineClient(config.predictionEngineUrl, config.accessKey);
pioPredictionEngineClient.setTimeout(5000);
var itemId = 'hid525675';
console.time('time');
pioPredictionEngineClient.sendRequest({ "items": [itemId], "num": 4 })
	.then(function(data) {
		console.timeEnd('time');
		console.log('done', data);
		console.log('source', 'http://hotels.itaka.pl/search?query_id='+itemId);
		_.each(data.itemScores, function(itemScore) {
			console.log('similar', 'http://hotels.itaka.pl/search?query_id='+itemScore.item);
		});
	
	})
	.fail(function(error){
		console.error(error);
	});
