'use strict';

var predictionClient = require('../index');

//Example config format
//{
//	"accessKey": "SomeVeryLongAccessKeyGeneratedByPioAppNew",
//	"url": "http://example.com:7070/"
//}
var config = require('./config.json');

// setup client
var pioEventServerClient = new predictionClient.PioEventServerClient(config.eventServerUrl, config.accessKey);
pioEventServerClient.setTimeout(5000);
var userToItemEvent = new predictionClient.UserToItemEvent(pioEventServerClient);

var user = {
	'id': 'user1',
	'options': {}
};

var product = {
	'id': 'item1',
	'options': {}
};
// send VIEW event to PIO EventServer
userToItemEvent
	.sendEvent( user, 'view', product )
	.then(function(eventId) {
		//duplicate event
		return userToItemEvent.sendEvent( user, 'view', product );
	})
	.then(function(eventId) {
		// check if really exists in event server?
		return pioEventServerClient.getEvent(eventId);
	})
	.then(function(data){
		console.log('event', data);
	})
	.fail(function(err){
		console.error('promise error catched', err, '\n', err.stack)
	})
	.done();

//userToItemEvent.on('send-success', function(data){
//	console.log('event send-success', data);
//});
//
//userToItemEvent.on('send-fail', function(data){
//	console.log('event send-fail', data);
//});
