'use strict';

var pioJs = require('../index');

//Example config format
//{
//	"accessKey": "SomeVeryLongAccessKeyGeneratedByPioAppNew",
//	"url": "http://example.com:7070/"
//}
var config = require('./config.json');

// setup client
var pioEventServerClient = new pioJs.PioEventServerClient(config.eventServerUrl, config.accessKey);
pioEventServerClient.setTimeout(5000);
var userEntity = new pioJs.UserEntity(pioEventServerClient);

var user = {
	'id': 'user11',
	'options': {
		properties: {
			name: 'John Doe2'
		}
	}
};

console.time('test-time');
console.timeEnd('test-time');
// send USER to PIO EventServer
userEntity.send(user.id, user.options)
	.then(function(eventId) {
		console.timeEnd('test-time');
		//duplicate event
		return userEntity.send(user.id, user.options);
	})
	.then(function(eventId) {
		console.timeEnd('test-time');
		// check if really exists in event server?
		return pioEventServerClient.getEvent(eventId);
	})
	.then(function(data){
		console.log('event', data);
	})
	.fail(function(err){
		console.error('promise error catched', err, '\n', err.stack);
	})
	.done(function(){
		console.timeEnd('test-time');
	});

//userToItemEvent.on('send-success', function(data){
//	console.log('event send-success', data);
//});
//
//userToItemEvent.on('send-fail', function(data){
//	console.log('event send-fail', data);
//});